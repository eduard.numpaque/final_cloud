"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.views import LoginView
from gestor_social.views import gestor_view,dependencias_edit,index,gestor_edit,dependencia_view,indexgestor,indexdependencias,login,gestor_delete,dependencias_delete
urlpatterns = [
    path('admin/', admin.site.urls),
    path('gestor_social/',LoginView.as_view(template_name='gestor_social/login.html'),name='login'),
    path('gestor_social/index_gestor/', gestor_view,name='gestor_crear'),
    path('gestor_social/index_dependecias/', dependencia_view,name='dependencia_crear'),
    path('gestor_social/index_borrargestor/<IdGestor>', gestor_delete,name='gestor_borrar'),
    path('gestor_social/index_borrardependencia/<IdDependencia>', dependencias_delete,name='dependencias_borrar'),
    path('gestor_social/index_editar/<IdGestor>', gestor_edit,name='index_editar'),
    path('gestor_social/index_editardep/<IdDependencia>', dependencias_edit,name='index_editardep'),
    path('gestor_social/index/', index,name='index'),

]

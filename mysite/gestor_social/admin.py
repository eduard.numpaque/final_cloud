from django.contrib import admin
from .models import Convocatorias
from .models import gestor_social
from .models import Decanatura
from .models import Dependencias
from .models import Evaluaciones
from .models import Sanciones
admin.site.register(Convocatorias),
admin.site.register(gestor_social),
admin.site.register(Decanatura),
admin.site.register(Dependencias),
admin.site.register(Evaluaciones),
admin.site.register(Sanciones)
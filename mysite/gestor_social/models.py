from django.db import models
class Decanatura(models.Model):
    IdDecanatura = models.AutoField(primary_key=True)
    Encargado = models.CharField(max_length=50)
    Correo = models.EmailField()
    password = models.CharField(max_length=20)


class Dependencias(models.Model):
        IdDependencia = models.AutoField(primary_key=True)
        Nombre = models.CharField(max_length=50)
        Solicitante = models.CharField(max_length=50)
        Correo = models.EmailField()
        Celular = models.CharField(max_length=10)
        password = models.CharField(max_length=20)
        Decanatura = models.ForeignKey(Decanatura, null=True, blank=True, on_delete=models.CASCADE, default=1)

class gestor_social(models.Model):
    IdGestor=models.AutoField(primary_key=True)
    Nombres = models.CharField(max_length=50)
    Apellidos=models.CharField(max_length=50)
    Carrera = models.CharField(max_length=100)
    Correo=models.EmailField()
    password=models.CharField(max_length=20)
    telefono=models.CharField(max_length=9)
    Celular=models.CharField(max_length=10)
class Convocatorias(models.Model):
    IdConvocatorias=models.AutoField(primary_key=True)
    Nombre = models.CharField(max_length=50)
    Fechadeinicio = models.DateTimeField()
    Fechadefin = models.DateTimeField()
    Encargado=models.CharField(max_length=50)
    Descripcion=models.CharField(max_length=200)
    HorasGanadas=models.IntegerField()
    NoEstudiantes=models.IntegerField()
    NoReserva = models.IntegerField()
    Dependencia=models.ForeignKey(Dependencias, null=True, blank=True, on_delete=models.CASCADE)


class Evaluaciones(models.Model):
    IdEvaluacion = models.AutoField(primary_key=True)
    Fecha = models.DateField()
    Dependencia = models.ForeignKey(Dependencias,null=True, blank=True,on_delete=models.CASCADE)
    Convocatoria = models.ForeignKey(Convocatorias, null=True, blank=True, on_delete=models.CASCADE)
    Gestor = models.ForeignKey(gestor_social, null=True, blank=True, on_delete=models.CASCADE)
    PresentacionPersonal=models.CharField(max_length=15)
    Disponibilidad=models.CharField(max_length=15)
    Puntualidad=models.CharField(max_length=15)
class Sanciones(models.Model):
    IdSancion = models.AutoField(primary_key=True)
    Descripcion = models.CharField(max_length=50)
    Solicitante = models.EmailField()
    Fechadeinicio = models.DateTimeField()
    Fechadefin = models.DateTimeField()
    Decanatura = models.ForeignKey(Decanatura, null=True, blank=True, on_delete=models.CASCADE)
    Gestor = models.ForeignKey(gestor_social, null=True, blank=True, on_delete=models.CASCADE)
    Convocatoria = models.ForeignKey(Convocatorias, null=True, blank=True, on_delete=models.CASCADE)


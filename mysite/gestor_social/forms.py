from django import forms
from .models import  gestor_social,Dependencias

class GestorForm(forms.ModelForm):
    class Meta:
        model=gestor_social
        fields= [

            'Nombres',
            'Apellidos',
            'Carrera',
            'Correo',
            'password',
            'telefono',
            'Celular',

        ]
        labels={

            'Nombres':'Nombres',
            'Apellidos':'Apellidos',
            'Carrera':'Carrera',
            'Correo':'Correo',
            'password':'password',
            'telefono':'telefono',
            'Celular':'Celular',
        }
        widgets={
            'Nombres':forms.TextInput(attrs={'id':'Nombres','class':'form-control','type':'text','placeholder':'Nombres','required':'required','autofocus':"autofocus"}),
            'Apellidos': forms.TextInput(attrs={'id':'Apellidos','class': 'form-control', 'type': 'text', 'placeholder': 'Apellidos', 'required': 'required','autofocus': "autofocus"}),

            'Carrera':forms.TextInput(attrs={'id':'Carrera','class':'form-control','type':'text','placeholder':'Carrera','required':'required','autofocus':"autofocus"}),
            'Correo':forms.TextInput(attrs={'id':'Correo','class':'form-control','type':'email','placeholder':'Correo','required':'required','autofocus':"autofocus"}),
            'password':forms.TextInput(attrs={'id':'password','class':'form-control','type':'password','placeholder':'password','required':'required','autofocus':"autofocus"}),
            'telefono':forms.TextInput(attrs={'id':'telefono','class':'form-control','type':'text','placeholder':'Telefono','required':'required','autofocus':"autofocus"}),
            'Celular':forms.TextInput(attrs={'id':'Celular','class':'form-control','type':'text','placeholder':'Celular','required':'required','autofocus':"autofocus"}),
        }


class DependenciasForm(forms.ModelForm):
    class Meta:
        model=Dependencias
        fields= [

            'Nombre',
            'Solicitante',
            'Correo',
            'Celular',
            'password',


        ]
        labels={

            'Nombre':'Nombre',
            'Solicitante':'Solicitante',
            'Correo':'Correo',
            'Celular':'Celular',
            'password':'password',

        }
        widgets={
            'Nombre':forms.TextInput(attrs={'id':'Nombre','class':'form-control','type':'text','placeholder':'Nombre','required':'required','autofocus':"autofocus"}),
            'Solicitante': forms.TextInput(attrs={'id':'Solicitante','class': 'form-control', 'type': 'text', 'placeholder': 'Solicitante', 'required': 'required','autofocus': "autofocus"}),
            'Correo':forms.TextInput(attrs={'id':'Correo','class':'form-control','type':'text','placeholder':'Correo','required':'required','autofocus':"autofocus"}),
            'Celular': forms.TextInput(attrs={'id': 'Celular', 'class': 'form-control', 'type': 'text', 'placeholder': 'Celular','required': 'required', 'autofocus': "autofocus"}),

            'password':forms.TextInput(attrs={'id':'password','class':'form-control','type':'password','placeholder':'password','required':'required','autofocus':"autofocus"}),
                    }
from django.shortcuts import render
# Create your views here.
from django.http import HttpResponse
from .forms import GestorForm,DependenciasForm
from .models import gestor_social,Dependencias
from django.shortcuts import redirect
def login(request):
    return render (request,"gestor_social/login.html")

def index(request):
    return render (request,"gestor_social/index.html")

def indexgestor(request):
    return render (request,"gestor_social/index_gestores.html")

def indexdependencias(request):
    return render (request,"gestor_social/index_dependencias.html")

def gestor_view(request):
    if request.method=='POST':
        form=GestorForm(request.POST)
        if form.is_valid():
            form.save()

    else:
        form=GestorForm()

    gestor = gestor_social.objects.all()

    return render (request,'gestor_social/index_gestores.html',{'form':form, 'gestor_social': gestor})




def dependencia_view(request):
    if request.method=='POST':
        form=DependenciasForm(request.POST)
        if form.is_valid():
            form.save()


    else:
        form=DependenciasForm()
    dependencia = Dependencias.objects.all()
    return render (request,'gestor_social/index_dependencias.html',{'form':form,'dependencias':dependencia})





def gestor_edit(request,IdGestor):
    gestor=gestor_social.objects.get(IdGestor=IdGestor)
    print("gestpr", gestor)
    if request.method =='GET':
        form=GestorForm(instance=gestor)
        print("form",form)
    else:
        form=GestorForm(request.POST, instance=gestor)
        if form.is_valid():
            form.save()
        return redirect('/gestor_social/index_gestor/')
    return render(request, 'gestor_social/index_editarGestor.html', {'form': form})




def gestor_delete(request,IdGestor):
    gestor=gestor_social.objects.get(IdGestor=IdGestor)

    gestor.delete()

    return redirect('/gestor_social/index_gestor/')

def dependencias_edit(request,IdDependencia):
    dependencia=Dependencias.objects.get(IdDependencia=IdDependencia)
    if request.method =='GET':
        form=DependenciasForm(instance=dependencia)
    else:
        form=GestorForm(resquest.POST, instance=dependencia)
        if form.is_valid():
            form.save()
        return redirect('gestor_social:index_gestor/')
    return render(request, 'gestor_social/index_editarDep.html', {'form': form})


def dependencias_delete(request,IdDependencia):
    dependencia=Dependencias.objects.get(IdDependencia=IdDependencia)

    dependencia.delete()

    return redirect('/gestor_social/index_dependecias/')